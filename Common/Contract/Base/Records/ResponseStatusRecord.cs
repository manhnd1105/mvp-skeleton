﻿using System.Runtime.Serialization;

namespace Common.Contract.Base
{
    [DataContract]
    public class ResponseStatusRecord
    {
        [DataMember(Name = "IsSuccessful", Order = 1, IsRequired = true)]
        public bool IsSuccessful { get; set; }

        [DataMember(Name = "ErrorMessage", Order = 2, IsRequired = false)]
        public string ErrorMessage { get; set; }
    }
}
