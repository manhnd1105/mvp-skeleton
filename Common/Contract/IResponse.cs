﻿using Common.Contract.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Contract
{
    public interface IResponse
    {
        ResponseStatusRecord Status { get; set; }
    }
}
