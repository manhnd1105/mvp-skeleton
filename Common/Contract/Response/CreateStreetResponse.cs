﻿
using Common.Contract.Base;
using System.Runtime.Serialization;

namespace Common.Contract
{
    [DataContract]
    public class CreateStreetResponse : IResponse
    {
        [DataMember]
        public ResponseStatusRecord Status { get; set; }

        [DataMember]
        public string Identifier { get; set; }
    }
}
