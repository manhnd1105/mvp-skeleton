﻿using System.Runtime.Serialization;

namespace Common.Contract
{
    [DataContract]
    public class DistrictDTO: DTOBase, IDataTransferObject
    {
        [DataMember]
        public string Identifier { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
