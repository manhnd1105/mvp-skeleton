﻿using System.Runtime.Serialization;

namespace Common.Contract
{
    [DataContract]
    public class StreetDTO : DTOBase, IDataTransferObject
    {
        [DataMember]
        public string Identifier { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string From { get; set; }

        [DataMember]
        public string To { get; set; }
    }
}
