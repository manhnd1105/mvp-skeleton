﻿
using Common.Contract.Base;
using Common.Contract.Base.Records;
using System.Runtime.Serialization;

namespace Common.Contract
{
    [DataContract]
    public class CreateStreetRequest: IRequest
    {
        [DataMember]
        public RequestHeaderRecord Header { get; set; }

        [DataMember]
        public StreetDTO Street { get; set; }

        [DataMember]
        public DistrictDTO District { get; set; }
    }
}
