﻿using System;
using System.Runtime.Caching;
using System.Text;

namespace Common.Cache
{
    public class CachedObject<T>
        where T : class
    {
        private static ObjectCache cache = MemoryCache.Default;
        public int CacheMinutes { get; set; }
        private string _customCacheItemName;
        public string CacheItemName
        {
            get
            {
                var stringBuilder = new StringBuilder(_customCacheItemName);
                bool startofString = true;
                foreach (object obj in CacheItemNameParts)
                {
                    if (startofString)
                    {
                        startofString = false;
                    }
                    else
                    {
                        stringBuilder.Append(SEPARATOR);
                    }
                    if (obj != null)
                    {
                        stringBuilder.Append(obj);
                    }
                }
                return stringBuilder.ToString().ToLower();
            }
        }
        public bool Cached { get; set; }
        public object[] CacheItemNameParts { get; set; }
        private static char SEPARATOR = '|';
        public CachedObject(int cacheMinutes, string customCacheItemName, params object[] cacheItemNameParts)
        {
            CacheMinutes = cacheMinutes;
            _customCacheItemName = customCacheItemName;
            CacheItemNameParts = cacheItemNameParts;
        }

        public T GetFromCache(Func<T> ifNotFound)
        {
            T obj;
            if (TryGetItem(CacheItemName, out obj))
            {
                return obj;
            }
            else
            {
                if (obj == default(T))
                {
                    obj = ifNotFound.Invoke();
                    StoreInCache(CacheItemName, obj);
                }
            }
            return obj;
        }

        private bool TryGetItem(string key, out T value)
        {
            value = cache[key] as T;
            if (value == null) return false;
            return true;
        }

        private void StoreInCache(string key, T data)
        {
            if (data != null)
            {
                cache.Add(key, data, DateTime.Now.AddMinutes(CacheMinutes));
            }
        }

        public void RemoveFromCache(string key)
        {
            cache.Remove(key);
        }

        public void RemoveFromCache()
        {
            RemoveFromCache(CacheItemName);
        }
    }
}
