﻿using NLog;
using System;

namespace BridgePattern.DataLayer.DataHelper
{
    public abstract class IIdentity
    {
        protected static Logger Log = LogManager.GetCurrentClassLogger();
        public string Prefix { get; set; }
        public virtual string ConvertIdToIdentifier(int Id)
        {
            return Prefix + Id.ToString();
        }

        public virtual int ConvertIdentifierToId(string Identifier)
        {
            var id = Identifier.Substring(Prefix.Length);
            return Convert.ToInt32(id);
        }
    }

    public class StreetIdentity: IIdentity
    {
        public StreetIdentity()
        {
            Prefix = "STR";
        }
    }

    public class DistrictIdentity: IIdentity
    {
        public DistrictIdentity()
        {
            Prefix = "DIS";
        }
    }
}
