﻿
namespace BridgePattern.DataLayer.DataAccessComponent.Repository.Base
{
    public interface IRepositoryFactory
    {
        IDistrictRepository Districts { get; }
        IStreetRepository Streets { get; }
    }
}
