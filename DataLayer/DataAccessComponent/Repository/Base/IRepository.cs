﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace BridgePattern.DataLayer.DataAccessComponent.Repository.Base
{
    public interface IRepository<TDataEntity>
        where TDataEntity : class
    {
        void Insert(TDataEntity entity, bool commit);
        void Update(TDataEntity entity, bool commit);
        void Delete(TDataEntity entity, bool commit);
        void Commit();
        IQueryable<TDataEntity> Find(Expression<Func<TDataEntity, bool>> predicate);
        IQueryable<TDataEntity> GetAll();
    }
}
