﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgePattern.DataLayer.DataAccessComponent.Repository.Base
{
    public class RepositoryFactory: IRepositoryFactory
    {
        public IDistrictRepository Districts { get { return new DistrictRepository(); } }
        public IStreetRepository Streets { get { return new StreetRepository(); } }
    }
}
