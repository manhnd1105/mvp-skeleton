﻿
using BridgePattern.DataLayer.DataAccessComponent.EntityFramework;
using NLog;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace BridgePattern.DataLayer.DataAccessComponent.Repository.Base
{
    public abstract class RepositoryBase<TDataEntity> : IRepository<TDataEntity>
        where TDataEntity: class
    {
        protected static Logger Log = LogManager.GetCurrentClassLogger();
        protected PccContainer Context { get; private set; }
        public RepositoryBase(PccContainer modelContext)
        {
            Context = modelContext;
        }

        public RepositoryBase()
        {
            //AutoMapper.Mapper.CreateMap<TDto, TDatabaseEntity>();
            //AutoMapper.Mapper.CreateMap<TDatabaseEntity, TDto>();
        }

        public void Insert(TDataEntity entity, bool commit)
        {

        }

        public void Update(TDataEntity entity, bool commit)
        {

        }

        public void Delete(TDataEntity entity, bool commit)
        {

        }

        public void Commit()
        {
            //Context.SaveChanges();
        }

        public IQueryable<TDataEntity> Find(Expression<Func<TDataEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TDataEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        //public List<TBusinessEntity> ConvertToBusinessEntity(List<TDto> dtoCollection)
        //{
        //    var result = new List<TBusinessEntity>();
        //    foreach (var item in dtoCollection)
        //    {
        //        var entity = new TBusinessEntity();
        //        entity.BackingEntity = item;
        //        result.Add(entity);
        //    }
        //    return result;
        //}

        //public List<TDto> ConvertToDto(List<TBusinessEntity> businessEntities)
        //{
        //    var result = new List<TDto>();
        //    foreach (var item in businessEntities)
        //    {
        //        result.Add((TDto)item.BackingEntity);
        //    }
        //    return result;
        //}

        //public List<TDatabaseEntity> ConvertToDatabaseEntity(List<TDto> dtoCollection)
        //{
        //    var result = new List<TDatabaseEntity>();
        //    foreach (var item in dtoCollection)
        //    {
        //        //result.Add(AutoMapper.Mapper.Map<TDatabaseEntity>(item));
        //    }
        //    return result;
        //}

        //public List<TDto> ConvertToDto(List<TDatabaseEntity> databaseEntities)
        //{
        //    var result = new List<TDto>();
        //    foreach (var item in databaseEntities)
        //    {
        //        //result.Add(AutoMapper.Mapper.Map<TDto>(item));
        //    }
        //    return result;
        //}
    }
}
