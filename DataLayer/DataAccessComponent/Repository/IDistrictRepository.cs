﻿
using BridgePattern.DataLayer.DataAccessComponent.EntityFramework;
using BridgePattern.DataLayer.DataAccessComponent.Repository.Base;
using System.Collections.Generic;

namespace BridgePattern.DataLayer.DataAccessComponent.Repository
{
    public interface IDistrictRepository: IRepository<District>
    {
        List<Street> FindAssociatedStreets(District district);
    }
}
