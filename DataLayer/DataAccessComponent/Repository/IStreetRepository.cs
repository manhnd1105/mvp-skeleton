﻿
using BridgePattern.DataLayer.DataAccessComponent.EntityFramework;
using BridgePattern.DataLayer.DataAccessComponent.Repository.Base;
using System.Collections.Generic;

namespace BridgePattern.DataLayer.DataAccessComponent.Repository
{
    public interface IStreetRepository: IRepository<Street>
    {
        List<District> FindAssociatedDistricts(Street street);
        bool AssignToDistrict(Street street, District district);
    }
}
