﻿
using BridgePattern.DataLayer.DataAccessComponent.EntityFramework;
using BridgePattern.DataLayer.DataAccessComponent.Repository.Base;
using System.Collections.Generic;

namespace BridgePattern.DataLayer.DataAccessComponent.Repository
{
    public class StreetRepository : RepositoryBase<Street>, IStreetRepository
    {
        public List<District> FindAssociatedDistricts(Street street)
        {
            return new List<District>();
        }
        public bool AssignToDistrict(Street street, District district)
        {
            return true;
        }
    }
}
