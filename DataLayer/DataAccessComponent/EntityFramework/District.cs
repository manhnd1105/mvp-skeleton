//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BridgePattern.DataLayer.DataAccessComponent.EntityFramework
{
    using System.Collections.Generic;
    
    public partial class District
    {
        public District()
        {
            this.Streets = new HashSet<Street>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<Street> Streets { get; set; }
    }
}
