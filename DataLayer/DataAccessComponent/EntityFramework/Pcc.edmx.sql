
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/24/2015 22:06:37
-- Generated from EDMX file: D:\workspace\mvp-skeleton\DataLayer\DataAccessComponent\EntityFramework\Pcc.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Pcc];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Districts'
CREATE TABLE [dbo].[Districts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Streets'
CREATE TABLE [dbo].[Streets] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [From] nvarchar(max)  NOT NULL,
    [To] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DistrictStreet'
CREATE TABLE [dbo].[DistrictStreet] (
    [Districts_Id] int  NOT NULL,
    [Streets_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Districts'
ALTER TABLE [dbo].[Districts]
ADD CONSTRAINT [PK_Districts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Streets'
ALTER TABLE [dbo].[Streets]
ADD CONSTRAINT [PK_Streets]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Districts_Id], [Streets_Id] in table 'DistrictStreet'
ALTER TABLE [dbo].[DistrictStreet]
ADD CONSTRAINT [PK_DistrictStreet]
    PRIMARY KEY CLUSTERED ([Districts_Id], [Streets_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Districts_Id] in table 'DistrictStreet'
ALTER TABLE [dbo].[DistrictStreet]
ADD CONSTRAINT [FK_DistrictStreet_District]
    FOREIGN KEY ([Districts_Id])
    REFERENCES [dbo].[Districts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Streets_Id] in table 'DistrictStreet'
ALTER TABLE [dbo].[DistrictStreet]
ADD CONSTRAINT [FK_DistrictStreet_Street]
    FOREIGN KEY ([Streets_Id])
    REFERENCES [dbo].[Streets]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DistrictStreet_Street'
CREATE INDEX [IX_FK_DistrictStreet_Street]
ON [dbo].[DistrictStreet]
    ([Streets_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------