﻿//using BridgePattern.DataLayer.DataAccessComponent;

namespace BridgePattern.DataLayer.ServiceGateway
{
    public interface IProviderFactory
    {
        //IRepositoryFactory DataRepository { get; }
        //IWorkflowFactory Workflow { get; }
        //IVelocityFactory Velocity { get; }
        IMediaProvider MediaProvider { get; }
    }
}
