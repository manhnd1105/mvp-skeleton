﻿
//using BridgePattern.DataLayer.DataAccessComponent;

namespace BridgePattern.DataLayer.ServiceGateway
{
    public class ProviderFactory : IProviderFactory
    {
        //public IRepositoryFactory DataRepository { get { return new RepositoryFactory(); }}
        public IMediaProvider MediaProvider { get { return new MediaProvider(); } }
        //public IWorkflowFactory Workflow { get { return new WorkflowFactory(); } }
        //public IVelocityFactory Velocity { get { return new VelocityFactory(); } }
    }
}
