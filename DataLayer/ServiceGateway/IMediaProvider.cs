﻿
namespace BridgePattern.DataLayer.ServiceGateway
{
    public interface IMediaProvider
    {
        string CreateDocument(byte[] stream);
    }
}
