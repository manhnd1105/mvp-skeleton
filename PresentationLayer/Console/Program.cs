﻿using Common.Contract;
using BridgePattern.ServiceLayer.Service;
using System;
using Common.Contract.Base;
using Common.Contract.Base.Records;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Run CreateStreet workflow to create a street use case
            var inputStreet = new StreetDTO() { Name = "Street5" };
            var inputDistrict = new DistrictDTO() { Identifier = "DIS3", Name = "District3", Description = "AAA" };

            var request = new CreateStreetRequest() { Street = inputStreet, District = inputDistrict, Header = new RequestHeaderRecord() { CallerName = "Main program" } };
            var response = new StreetService().CreateStreet(request);

            //Display result on UI
            //var view = new CreateStreetViewBackend();
            switch (response.Status.IsSuccessful)
            {
                case true:
                    Console.WriteLine(string.Format("A street {0} was created", inputStreet.Name));
                    break;
                case false:
                    Console.WriteLine("Error. District was not created");
                    break;
            }
            Console.ReadKey();
        }
    }
}
