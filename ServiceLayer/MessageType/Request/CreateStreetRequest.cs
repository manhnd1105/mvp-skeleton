﻿
using BridgePattern.ServiceLayer.MessageType.Record;
using System.Runtime.Serialization;

namespace BridgePattern.ServiceLayer.MessageType.Request
{
    [DataContract]
    public class CreateStreetRequest: IRequest
    {
        [DataMember]
        public RequestHeaderRecord Header { get; set; }

        [DataMember]
        public StreetDTO Street { get; set; }

        [DataMember]
        public DistrictDTO District { get; set; }
    }
}
