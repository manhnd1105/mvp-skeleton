﻿
using System.Runtime.Serialization;

namespace BridgePattern.ServiceLayer.MessageType.Response
{
    [DataContract]
    public class CreateStreetResponse : IResponse
    {
        [DataMember]
        public ResponseStatusRecord Status { get; set; }

        [DataMember]
        public string Identifier { get; set; }
    }
}
