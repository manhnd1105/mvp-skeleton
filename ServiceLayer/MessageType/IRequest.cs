﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgePattern.ServiceLayer.MessageType
{
    public interface IRequest
    {
        RequestHeaderRecord Header { get; set; }
    }
}
