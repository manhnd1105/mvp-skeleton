﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BridgePattern.ServiceLayer.MessageType
{
    [DataContract]
    public class RequestHeaderRecord
    {
        [DataMember(Name = "CallerName", Order = 1, IsRequired = true)]
        public string CallerName { get; set; }
    }
}
