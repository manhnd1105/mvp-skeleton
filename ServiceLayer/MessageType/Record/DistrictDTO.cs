﻿using System.Runtime.Serialization;

namespace BridgePattern.ServiceLayer.MessageType.Record
{
    [DataContract]
    public class DistrictDTO: DTOBase, IDataTransferObject
    {
        [DataMember]
        public string Identifier { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
