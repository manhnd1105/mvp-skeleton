﻿using System.Runtime.Serialization;

namespace BridgePattern.ServiceLayer.MessageType.Record
{
    [DataContract]
    public class StreetDTO : DTOBase, IDataTransferObject
    {
        [DataMember]
        public string Identifier { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string From { get; set; }

        [DataMember]
        public string To { get; set; }
    }
}
