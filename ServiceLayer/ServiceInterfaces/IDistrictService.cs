﻿using System.ServiceModel;

namespace BridgePattern.ServiceLayer.ServiceInterface
{
    [ServiceContract]
    public interface IDistrictService: IService
    {

    }
}
