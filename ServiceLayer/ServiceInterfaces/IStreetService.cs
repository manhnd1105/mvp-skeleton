﻿
using Common.Contract;
using System.ServiceModel;

namespace BridgePattern.ServiceLayer.ServiceInterface
{
    [ServiceContract]
    public interface IStreetService: IService
    {
        [OperationContract]
        CreateStreetResponse CreateStreet(CreateStreetRequest request);
    }
}
