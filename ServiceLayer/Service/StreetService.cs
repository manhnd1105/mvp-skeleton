﻿
using BridgePattern.BusinessLayer.BusinessWorkflow;
using BridgePattern.ServiceLayer.Service.Base;
using BridgePattern.ServiceLayer.ServiceInterface;
using Common.Contract;
namespace BridgePattern.ServiceLayer.Service
{
    public class StreetService: ServiceBase, IService, IStreetService
    {
        public CreateStreetResponse CreateStreet(CreateStreetRequest request)
        {
            return new CreateStreetWorkflow().Execute(request);
        }
    }
}
