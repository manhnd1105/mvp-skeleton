﻿
using BridgePattern.ServiceLayer.Service.Base;
using BridgePattern.ServiceLayer.ServiceInterface;
namespace BridgePattern.ServiceLayer.Service
{
    public class DistrictService : ServiceBase, IService, IDistrictService
    {
        //public IProviderFactory ProviderFactory { get; set; }
    }
}
