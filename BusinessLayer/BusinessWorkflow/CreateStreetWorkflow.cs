﻿

using BridgePattern.BusinessLayer.BusinessEntity;
using BridgePattern.BusinessLayer.BusinessEntity.Entity;
using BridgePattern.BusinessLayer.BusinessWorkflow.Base;
using BridgePattern.DataLayer.DataAccessComponent.Repository.Base;
using BridgePattern.DataLayer.ServiceGateway;
using Common.Contract;
using System;


namespace BridgePattern.BusinessLayer.BusinessWorkflow
{
    public class CreateStreetWorkflow : BusinessWorkflowCommandBase<CreateStreetRequest, CreateStreetResponse>, IBusinessWorkflow<CreateStreetRequest, CreateStreetResponse>
    {
        public StreetDTO StreetDTO { get; set; }
        public DistrictDTO DistrictDTO { get; set; }
        public CreateStreetWorkflow(): base(new RepositoryFactory())
        {

        }

        //Choose sequential workflow
        //Create a street that associates with a district
        protected override bool OnExecute(CreateStreetResponse response)
        {
            //1.Check velocity on District must contain streets
            var status = true;
            
            //2.Create a street only if it can pass business velocity
            int responseCode = (int)WorkflowStatus.Error;
            
            if (status)
            {
                responseCode = CreateStreetAndAssignToDistrict();
            }
            response.Identifier = "STR1";
            response.Status.IsSuccessful = true;
            return true;
        }

        protected override void Validate(CreateStreetRequest request)
        {
            //Map the contract records with business entities
            StreetDTO = request.Street;
            DistrictDTO = request.District;
            //Street = new Street() { Name = request.Street.Name, From = request.Street.From, To = request.Street.To };
            //District = new District() { Name = request.District.Name, Description = request.District.Description};
        }

        private int CreateStreetAndAssignToDistrict()
        {
            try
            {
                //RepositoryFactory.Streets.Insert(Street, true);
                //RepositoryFactory.Streets.AssignToDistrict(Street, District);
                //RepositoryFactory.Streets.Commit();
                return (int)WorkflowStatus.OK;
            }
            catch (Exception e)
            {
                Log.Error(e, "CreateStreetWorkflow");
                return (int)WorkflowStatus.Error;
            }
            
        }
    }
}
