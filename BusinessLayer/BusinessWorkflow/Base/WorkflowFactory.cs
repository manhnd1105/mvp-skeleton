﻿
using BridgePattern.BusinessLayer.BusinessWorkflow.Base;
using Common.Contract;
namespace BridgePattern.BusinessLayer.BusinessWorkflow
{
    public class WorkflowFactory: IWorkflowFactory
    {
        public WorkflowFactory()
        {
            //CreateStreet = new CreateStreetWorkflow();
        }

        public IBusinessWorkflow<CreateStreetRequest, CreateStreetResponse> CreateStreet { get; private set; }
    }
}
