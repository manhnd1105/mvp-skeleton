﻿
using BridgePattern.BusinessLayer.BusinessWorkflow.Base;
using Common.Contract;
namespace BridgePattern.BusinessLayer.BusinessWorkflow
{
    public interface IWorkflowFactory
    {
        IBusinessWorkflow<CreateStreetRequest, CreateStreetResponse> CreateStreet { get; }

    }
}
