﻿using Common.Contract;
using Common.Contract.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgePattern.BusinessLayer.BusinessWorkflow.Base
{
    public interface IBusinessWorkflow<TRequest, TResponse>
        where TRequest : IRequest
        where TResponse : IResponse, new()
    {
        TResponse Execute(TRequest request);
    }
}
