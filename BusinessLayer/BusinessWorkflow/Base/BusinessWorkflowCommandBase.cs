﻿
using BridgePattern.DataLayer.DataAccessComponent.Repository.Base;
using BridgePattern.DataLayer.ServiceGateway;
using Common.Contract;
using Common.Contract.Base;
using NLog;
using System;

namespace BridgePattern.BusinessLayer.BusinessWorkflow
{
    public abstract class BusinessWorkflowCommandBase<TRequest, TResponse> : CommandBase<TRequest, TResponse>
        where TRequest : IRequest
        where TResponse : IResponse, new()
    {
        public IRepositoryFactory RepositoryFactory { get; set; }
        protected BusinessWorkflowCommandBase(IRepositoryFactory factory)
        {
            RepositoryFactory = factory;
        }
    }
}
