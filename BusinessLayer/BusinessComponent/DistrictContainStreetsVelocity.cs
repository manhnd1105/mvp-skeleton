﻿
using BridgePattern.BusinessLayer.BusinessComponent.Contract.Request;
using BridgePattern.BusinessLayer.BusinessComponent.Contract.Response;
using BridgePattern.BusinessLayer.BusinessEntity;
using BridgePattern.BusinessLayer.BusinessEntity.Entity;
using BridgePattern.BusinessLayer.BusinessRepository;
using Common.Contract;
using Common.Contract.Base;
using System.Collections.Generic;

namespace BridgePattern.BusinessLayer.BusinessComponent
{
    //Velocity
    public class DistrictContainStreetsVelocity : 
        CommandBase<DistrictContainStreetsVelocityRequest, DistrictContainStreetsVelocityResponse>
    {
        private const int MaxDistrictNumber = 50;
        public District District { get; set; }
        public Street Street { get; set; }

        protected override void Validate(DistrictContainStreetsVelocityRequest request)
        {
            Street = request.Street;
            District = request.District;
        }

        protected override bool OnExecute(DistrictContainStreetsVelocityResponse response)
        {
            //A district must contain > 0 and < 50 streets
            var streets = new Common.Cache.CachedObject<List<Street>>(10, "FindAssociatedStreets")
                .GetFromCache(() =>
                {
                    return new RepositoryFactory().Districts.FindAssociatedStreets(District);
                });
            if (streets.Count < 0 || streets.Count > MaxDistrictNumber)
            {
                return false;
            }
            return true;
        }
    }
}
