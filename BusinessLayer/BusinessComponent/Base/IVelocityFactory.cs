﻿
namespace BridgePattern.BusinessLayer.BusinessComponent
{
    public interface IVelocityFactory
    {
        DistrictContainStreetsVelocity DistrictContainStreetsVelocity { get; }
    }
}
