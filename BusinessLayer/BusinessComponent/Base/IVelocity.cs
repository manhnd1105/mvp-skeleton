﻿using BridgePattern.BusinessLayer.BusinessEntity;
using Common.Contract;

namespace BridgePattern.BusinessLayer.BusinessComponent
{
    public interface IVelocity
    {
        bool Execute(IBusinessEntity<IDataTransferObject> entity);
    }
}
