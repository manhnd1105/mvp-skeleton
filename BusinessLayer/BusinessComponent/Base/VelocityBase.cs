﻿

using BridgePattern.BusinessLayer.BusinessRepository;
using Common.Contract.Base;
using NLog;
namespace BridgePattern.BusinessLayer.BusinessComponent
{
    public abstract class VelocityBase<TRequest, TResponse>
        where TRequest: IRequest
        where TResponse: IResponse
    {
        protected static Logger Log = LogManager.GetCurrentClassLogger();
        public RepositoryFactory BusinessRepository { get { return new RepositoryFactory(); } }
        //public IProviderFactory provider { get; private set; }

        //public VelocityBase()
        //{
        //    provider = new ProviderFactory();
        //}
    }
}
