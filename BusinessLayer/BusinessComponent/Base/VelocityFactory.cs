﻿
namespace BridgePattern.BusinessLayer.BusinessComponent
{
    public class VelocityFactory : IVelocityFactory
    {
        public DistrictContainStreetsVelocity DistrictContainStreetsVelocity { get { return new DistrictContainStreetsVelocity(); } }
    }
}
