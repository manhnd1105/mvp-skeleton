﻿using BridgePattern.BusinessLayer.BusinessEntity.Entity;
using Common.Contract.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgePattern.BusinessLayer.BusinessComponent.Contract.Request
{
    public class DistrictContainStreetsVelocityRequest : IRequest
    {
        public RequestHeaderRecord Header { get; set; }
        public District District { get; set; }
        public Street Street { get; set; }
    }
}
