﻿using Common.Contract.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgePattern.BusinessLayer.BusinessComponent.Contract.Response
{
    public class DistrictContainStreetsVelocityResponse: IResponse
    {
        public ResponseStatusRecord Status { get; set; }
    }
}
