﻿using BridgePattern.BusinessLayer.BusinessEntity.Entity;
using System.Collections.Generic;

namespace BridgePattern.BusinessLayer.BusinessRepository
{
    public interface IStreetRepository: IRepository<Street>
    {
        List<District> FindAssociatedDistricts(Street street);
        bool AssignToDistrict(Street street, District district);
    }
}
