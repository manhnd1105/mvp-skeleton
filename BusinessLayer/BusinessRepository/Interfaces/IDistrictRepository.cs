﻿using BridgePattern.BusinessLayer.BusinessEntity.Entity;
using System.Collections.Generic;

namespace BridgePattern.BusinessLayer.BusinessRepository
{
    public interface IDistrictRepository: IRepository<District>
    {
        List<Street> FindAssociatedStreets(District district);
    }
}
