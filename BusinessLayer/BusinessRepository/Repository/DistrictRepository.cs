﻿using BridgePattern.BusinessLayer.BusinessEntity.Entity;
using BridgePattern.DataLayer.DataHelper;
using Common.Contract;
using System.Collections.Generic;

namespace BridgePattern.BusinessLayer.BusinessRepository
{
    class DistrictRepository: RepositoryBase<District>, IDistrictRepository
    {
        public DistrictRepository(PccContainer modelContext): base(modelContext)
        {

        }

        public List<Street> FindAssociatedStreets(District district)
        {
            //Find district id from business object
            var identity = new DistrictIdentity();
            var districtDTO = (DistrictDTO)district.BackingEntity;
            
            int districtId = identity.ConvertIdentifierToId(districtDTO.Identifier);

            //Query database
            



            //Return response in business format
            var responseDTO = new List<StreetDTO>();

            var result = new List<Street>();
            //foreach (var item in responseDTO)
            //{
            //    result.Add(new Street(item));
            //}
            return result;
        }
    }
}
