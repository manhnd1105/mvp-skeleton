﻿using BridgePattern.BusinessLayer.BusinessEntity.Entity;
using System.Collections.Generic;

namespace BridgePattern.BusinessLayer.BusinessRepository
{
    public class StreetRepository : RepositoryBase<Street>, IStreetRepository
    {
        public StreetRepository(PccContainer modelContext): base(modelContext)
        {

        }
        public List<District> FindAssociatedDistricts(Street street)
        {
            return new List<District>();
        }
        public bool AssignToDistrict(Street street, District district)
        {
            return true;
        }
    }
}
