﻿
using BridgePattern.BusinessLayer.BusinessEntity.Entity;
namespace BridgePattern.BusinessLayer.BusinessRepository
{
    public class RepositoryFactory: IRepositoryFactory
    {
        public IDistrictRepository Districts { get; private set; }
        public IStreetRepository Streets { get; private set; }

        public PccContainer ModelContext { get { return new PccContainer(); } }

        public RepositoryFactory()
        {
            Districts = new DistrictRepository(ModelContext);
            Streets = new StreetRepository(ModelContext);
        }
    }
}
