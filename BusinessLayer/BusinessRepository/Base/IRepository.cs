﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace BridgePattern.BusinessLayer.BusinessRepository
{
    public interface IRepository<TBusinessEntity>
        //where TBusinessEntity : IBusinessEntity<IDataTransferObject>
    {
        void Insert(TBusinessEntity entity, bool commit);
        void Update(TBusinessEntity entity, bool commit);
        void Delete(TBusinessEntity entity, bool commit);
        void Commit();
        IQueryable<TBusinessEntity> Find(Expression<Func<TBusinessEntity, bool>> predicate);
        IQueryable<TBusinessEntity> GetAll();
    }
}
