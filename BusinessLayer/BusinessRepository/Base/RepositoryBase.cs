﻿using BridgePattern.BusinessLayer.BusinessEntity;
using BridgePattern.BusinessLayer.BusinessEntity.Entity;
using NLog;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace BridgePattern.BusinessLayer.BusinessRepository
{
    public abstract class RepositoryBase<TBusinessEntity> : IRepository<TBusinessEntity>
        //where TDto: IDataTransferObject
        where TBusinessEntity : BusinessEntityBase
        //where TDatabaseEntity: class
    {
        //protected PccContainer Context { get { return new PccContainer(); } }
        protected static Logger Log = LogManager.GetCurrentClassLogger();
        public PccContainer Context { get; set; }
        public RepositoryBase(PccContainer modelContext)
        {
            Context = modelContext;
        }

        public RepositoryBase()
        {
            //AutoMapper.Mapper.CreateMap<TDto, TDatabaseEntity>();
            //AutoMapper.Mapper.CreateMap<TDatabaseEntity, TDto>();
        }

        public void Insert(TBusinessEntity entity, bool commit)
        {

        }

        public void Update(TBusinessEntity entity, bool commit)
        {

        }

        public void Delete(TBusinessEntity entity, bool commit)
        {

        }

        public void Commit()
        {
            //Context.SaveChanges();
        }

        public IQueryable<TBusinessEntity> Find(Expression<Func<TBusinessEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TBusinessEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        //public List<TBusinessEntity> ConvertToBusinessEntity(List<TDto> dtoCollection)
        //{
        //    var result = new List<TBusinessEntity>();
        //    foreach (var item in dtoCollection)
        //    {
        //        var entity = new TBusinessEntity();
        //        entity.BackingEntity = item;
        //        result.Add(entity);
        //    }
        //    return result;
        //}

        //public List<TDto> ConvertToDto(List<TBusinessEntity> businessEntities)
        //{
        //    var result = new List<TDto>();
        //    foreach (var item in businessEntities)
        //    {
        //        result.Add((TDto)item.BackingEntity);
        //    }
        //    return result;
        //}

        //public List<TDatabaseEntity> ConvertToDatabaseEntity(List<TDto> dtoCollection)
        //{
        //    var result = new List<TDatabaseEntity>();
        //    foreach (var item in dtoCollection)
        //    {
        //        //result.Add(AutoMapper.Mapper.Map<TDatabaseEntity>(item));
        //    }
        //    return result;
        //}

        //public List<TDto> ConvertToDto(List<TDatabaseEntity> databaseEntities)
        //{
        //    var result = new List<TDto>();
        //    foreach (var item in databaseEntities)
        //    {
        //        //result.Add(AutoMapper.Mapper.Map<TDto>(item));
        //    }
        //    return result;
        //}
    }
}
