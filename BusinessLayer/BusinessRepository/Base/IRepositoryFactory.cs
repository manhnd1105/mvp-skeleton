﻿
namespace BridgePattern.BusinessLayer.BusinessRepository
{
    public interface IRepositoryFactory
    {
        IDistrictRepository Districts { get; }
        IStreetRepository Streets { get; }
    }
}
