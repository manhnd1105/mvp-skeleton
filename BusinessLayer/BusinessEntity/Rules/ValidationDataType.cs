﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BridgePattern.BusinessLayer.BusinessEntity.Rules
{
   
    // datatype enum of types to be used in validation rules
    
    public enum ValidationDataType
    {
        String, 
        Integer,
        Double, 
        Decimal,
        Date
    }
}
