﻿using BridgePattern.BusinessLayer.BusinessEntity;
using Common.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgePattern.BusinessLayer.BusinessEntity.Rules
{
    public interface IBusinessRule
    {
        string Error { get; set; }
        bool Validate(IBusinessEntity<IDataTransferObject> businessObject);
    }
}
