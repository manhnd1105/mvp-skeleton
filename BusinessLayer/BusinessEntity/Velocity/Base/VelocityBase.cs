﻿


using BridgePattern.DataLayer.DataAccessComponent.Repository.Base;
using Common.Contract.Base;
using NLog;
namespace BridgePattern.BusinessLayer.BusinessEntity.Velocity.Base
{
    public abstract class VelocityBase
    {
        protected static Logger Log = LogManager.GetCurrentClassLogger();
        public RepositoryFactory RepositoryFactory { get { return new RepositoryFactory(); } }
        //public IProviderFactory provider { get; private set; }

        //public VelocityBase()
        //{
        //    provider = new ProviderFactory();
        //}
    }
}
