﻿using BridgePattern.BusinessLayer.BusinessEntity;
using Common.Contract;

namespace BridgePattern.BusinessLayer.BusinessEntity.Velocity.Base
{
    public interface IVelocity
    {
        bool Execute();
    }
}
