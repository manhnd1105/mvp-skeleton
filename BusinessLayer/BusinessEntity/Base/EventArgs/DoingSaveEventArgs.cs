﻿using System;

namespace BridgePattern.BusinessLayer.BusinessEntity
{
    public class DoingSaveEventArgs : EventArgs
    {
        public string Name { get; set; }
    }
}
