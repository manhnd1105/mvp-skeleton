﻿using System;

namespace BridgePattern.BusinessLayer.BusinessEntity
{
    public interface IParentEventBag
    {
        //Raised when the parent's Save() is called
        event EventHandler<DoingSaveEventArgs> DoingSaveEventArgs;

        //Occurs when the Save() was successful for all children
        event EventHandler<DoSaveSuccessEventArgs> DoSaveSuccessEventArgs;

        //Raised if the parent encounters a when trying to save or delete
        event EventHandler<DoSaveFailedEventArgs> DoSaveFailedEventArgs;
    }
}
