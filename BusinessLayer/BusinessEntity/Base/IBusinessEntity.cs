﻿
using Common.Contract;
using System.Collections.Generic;

namespace BridgePattern.BusinessLayer.BusinessEntity
{
    public interface IBusinessEntity<T>
        where T: IDataTransferObject
    {
        //public E BackingEntity { get; private set; }
        T BackingEntity { get; set; }
        List<string> Validate();
        
    }
}
