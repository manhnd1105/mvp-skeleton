﻿
using BridgePattern.BusinessLayer.BusinessEntity.Rules;
using Common.Contract;
using NLog;
using System;
using System.Collections.Generic;

namespace BridgePattern.BusinessLayer.BusinessEntity
{
    public abstract class BusinessEntityBase : IBusinessEntity<IDataTransferObject>
    {
        #region "Validation"
        public List<string> ValidationErrors { get; private set; }
        protected static Logger Log = LogManager.GetCurrentClassLogger();
        public virtual bool IsValid
        {
            get
            {
                bool valid = true;
                ValidationErrors.Clear();
                foreach (var item in Rules)
                {
                    if (!item.Validate(this))
                    {
                        valid = false;
                        ValidationErrors.Add(item.Error);
                    }
                }
                return valid;
            }
            set
            {
                this.IsValid = value;
            }
        }
        public virtual List<string> Validate() { return new List<string>(); }
        public List<IBusinessRule> Rules { get; set; }
        public void AddRule(IBusinessRule rule)
        {
            Rules.Add(rule);
        }
        #endregion
        #region "Constructor"
        public BusinessEntityBase()
        {

        }

        public BusinessEntityBase(IDataTransferObject backingEntity)
        {
            BackingEntity = backingEntity;
        }
        public BusinessEntityBase(BusinessEntityBase parent, IParentEventBag parentEventBag) : this ()
        {
            Parent = parent;
            ParentEventBag = parentEventBag;

            parentEventBag.DoingSaveEventArgs += new EventHandler<DoingSaveEventArgs>(DoingSaveEventHandler);
            parentEventBag.DoSaveSuccessEventArgs += new EventHandler<DoSaveSuccessEventArgs>(DoSaveSuccessEventHandler);
            parentEventBag.DoSaveFailedEventArgs += new EventHandler<DoSaveFailedEventArgs>(DoSaveFailedEventHandler);
        }
        #endregion
        #region "Persistence calls"
        public IDataTransferObject BackingEntity { get; set; }
        public virtual bool IsDirty { get; set; }
        public virtual bool IsNew { get; set; }
        public virtual bool IsDeleted { get; set; }
        #endregion
        #region "Children calls"
        public List<BusinessEntityBase> Children { get; set; }
        public virtual bool AreChildrenDirty
        {
            get
            {
                foreach (var child in Children)
                {
                    if (child.IsDirty)
                    {
                        return true;
                    } 
                }
                return false;
            }
        }
        public virtual bool AreChildrenValid
        {
            get
            {
                foreach (var child in Children)
                {
                    //if (!child.IsValid)
                    //{
                    //    return false;
                    //}
                }
                return true;
            }
        }
        protected void AddChild(BusinessEntityBase child)
        {
            if (child == null)
            {
                throw new ArgumentNullException("child", "Child object must not be null");
            }
            Children.Add(child);
        }
        #endregion
        #region "Parent calls"
        public BusinessEntityBase Parent { get; set; }
        public IParentEventBag ParentEventBag { get; set; }
        public virtual void DoingSaveEventHandler(object sender, DoingSaveEventArgs args)
        {

        }
        public virtual void DoSaveFailedEventHandler(object sender, DoSaveFailedEventArgs args)
        {

        }
        public virtual void DoSaveSuccessEventHandler(object sender, DoSaveSuccessEventArgs args)
        {

        }
        #endregion
        #region "Events"
        public event EventHandler<DoingSaveEventArgs> DoingSaveEvent;
        public event EventHandler<DoSaveSuccessEventArgs> DoSaveSuccessEvent;
        public event EventHandler<DoSaveFailedEventArgs> DoSaveFailedEvent;
        public event EventHandler<EventArgs> AfterFetchEvent;
        public event EventHandler<EventHandler> BeforeFetchEvent;
        #endregion
        #region "Raise event calls"
        public void OnDoingSaveEvent(object sender, DoingSaveEventArgs args)
        {
            if (DoingSaveEvent != null)
            {
                foreach (Delegate delegateCall in DoingSaveEvent.GetInvocationList())
                {
                    delegateCall.DynamicInvoke(sender, args);
                    
                }
            }
        }
        public void OnDoSaveSuccessEvent(object sender, DoSaveSuccessEventArgs args)
        {
            if (DoSaveSuccessEvent != null)
            {
                foreach (Delegate delegateCall in DoSaveSuccessEvent.GetInvocationList())
                {
                    delegateCall.DynamicInvoke(sender, args);
                }
            }
        }
        public void OnDoSaveFailedEvent(object sender, DoSaveFailedEventArgs args)
        {
            if (DoSaveFailedEvent != null)
            {
                foreach (Delegate delegateCall in DoSaveFailedEvent.GetInvocationList())
                {
                    delegateCall.DynamicInvoke(sender, args);
                }
            }
        }
        public void OnAfterFetchEvent(object sender, EventArgs args)
        {
            if (AfterFetchEvent != null)
            {
                foreach (Delegate delegateCall in AfterFetchEvent.GetInvocationList())
                {
                    delegateCall.DynamicInvoke(sender, args);
                }
            }
        }
        public void OnBeforeFetchEvent(object sender, EventArgs args)
        {
            if (BeforeFetchEvent != null)
            {
                foreach (Delegate delegateCall in BeforeFetchEvent.GetInvocationList())
                {
                    delegateCall.DynamicInvoke(sender, args);
                }
            }
        }
        #endregion
    }
}
